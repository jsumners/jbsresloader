package com.jrfom.util;

import org.junit.Test;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.util.HashMap;

import static junit.framework.Assert.assertEquals;

public class ResloaderTest {
  @Test
  public void testLoadJson() throws Exception {
    HashMap<String, String> json =
        (HashMap<String, String>)
            Resloader.loadJson(ResloaderTest.class, "test.json");
    assertEquals(json.get("foo"), "bar");
  }

  @Test
  public void testLoadJsonAsType() throws Exception {
    Bar bar = Resloader.loadJson(ResloaderTest.class, "test.json", Bar.class);
    assertEquals(bar.getFoo(), "bar");
  }

  @Test
  public void testLoadString() throws Exception {
    String data = Resloader.loadString(ResloaderTest.class, "test.json");
    assertEquals(data, "{\"foo\":\"bar\"}");
  }

  @Test
  public void testLoadData() throws Exception {
    // sigil.png is from
    // http://awoiaf.westeros.org/index.php/File:Greyjoy_coat_sigil.png
    byte[] data = Resloader.loadData(ResloaderTest.class, "sigil.png");

    MessageDigest md = MessageDigest.getInstance("SHA-1");
    md.update(data);
    byte[] dataHash = md.digest();

    StringBuilder dataHashHex = new StringBuilder();
    for (int i = 0, j = dataHash.length; i < j; i += 1) {
      dataHashHex.append(String.format("%2x", dataHash[i]));
    }

    assertEquals(
        dataHashHex.toString(),
        "cf382d9eb1d823ad93689b68e968f9e69086ecb4"
    );

    // Let's also test the loadFile(File) method directly.
    File tmpFile = File.createTempFile("sigil", "png", null);
    tmpFile.setWritable(true);
    DataOutputStream dos = new DataOutputStream(new FileOutputStream(tmpFile));
    dos.write(data);
    dos.close();

    data = Resloader.loadFile(tmpFile);
    dataHashHex.delete(0, dataHashHex.length());
    md.update(data);
    dataHash = md.digest();
    for (int i = 0, j = dataHash.length; i < j; i += 1) {
      dataHashHex.append(String.format("%2x", dataHash[i]));
    }

    assertEquals(
        dataHashHex.toString(),
        "cf382d9eb1d823ad93689b68e968f9e69086ecb4"
    );
  }

  @Test
  public void testJarLoad() throws Exception {
    HashMap<String, String> json =
        (HashMap<String, String>)
            Resloader.loadJson(ResloaderTest.class, "jar.json");
    assertEquals(json.get("foo"), "bar");
  }
}
