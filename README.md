# Resloader #

Resloader is a simple class for loading JSON, and other types of, files within a Java application's classpath as resources. It uses [Google's GSON](https://code.google.com/p/google-gson/) for loading JSON as known Java objects and [json-simple](https://code.google.com/p/json-simple/) for loading JSON as generic Java objects.

## Examples ##

You can view the projects tests for "working" examples. The following examples are adapted from the tests just to give a brief introduction.

### Generic Java Object ###

Let's assume you have a JSON file, `bar.json`, with the contents:

    {"foo":"bar"}

Let's further assume that you need to load this file from your `com.example.Awesome` class *and* `bar.json` is a sibling file of the `Awesome.class` file. Then you can use `Resloader` like so:

    HashMap<String, String> bar =
      (HashMap<String, String>)Resloader.loadJson(Awesome.class, "bar.json");
    
    System.out.println("bar.foo = " + bar.get("foo"));

### Known Java Object ###

Given the assumptions from "Generic Java Object", let's assume you have a `com.example.Bar` class:

    public class Bar {
      private String foo;
      
      public String getFoo() {
        return foo;
      }
      
      public void setFoo(String foo) {
        this.foo = foo;
      }
    }

Then you can use `Resloader` like so:

    Bar bar = Resloader.loadJson(Awesome.class, "bar.json", Bar.class);
    
    System.out.println("bar.foo = " + bar.getFoo());

### Just The Text Please ###

    String data = Resloader.loadString(Awesome.class, "bar.json");
    
    System.out.println(data); // {"foo":"bar"}

## License ##

The MIT License (MIT)

Copyright (c) 2013 James Sumners

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.