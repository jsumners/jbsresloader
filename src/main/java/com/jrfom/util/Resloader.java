package com.jrfom.util;

import com.google.gson.Gson;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.jar.JarInputStream;
import java.util.zip.ZipEntry;

/**
 * <p>A utility class for loading resource files from the classpath.
 * Specifically, resources are assumed to be siblings of a given {@link Class}
 * or in a descendent directory of the {@link Class}'s location.</p>
 *
 * <p>Resource files's locations are determined by taking the package name of
 * the given {@link Class}, converting it to a path, and appending the given
 * resource name. For example, given the class {@code Foo} in the package
 * "com.example", and the resource name "bar.text", {@link Resloader} will use
 * "com/example/bar.text" to load the resource.</p>
 */
public class Resloader {
  private static Logger log = LoggerFactory.getLogger(Resloader.class);

  /**
   * Loads a resource file as binary data and returns the result. The file size
   * cannot exceed {@link Integer.MAX_VALUE}.
   *
   * @param clazz The {@link Class} to use for a base path.
   * @param dataFile The file whose data will be read in.
   * @return A {@code byte[]} of the file data.
   * @throws IOException
   */
  public static byte[] loadData(Class clazz, String dataFile)
    throws IOException
  {
    log.debug("loadData(class, dataFile)");
    log.debug("dataFile: {}", dataFile);

    File resourceFile = resourceFile(resourceUri(clazz, dataFile));
    return Resloader.loadFile(resourceFile);
  }

  /**
   * <p>Loads any given {@link File} into a {@code byte[]} and returns it.
   * This method <em>does not</em> attempt to locate the resource within
   * the class path. It assumes the given {@linkplain File} is valid and
   * exists.</p>
   *
   * @param file The {@link File} to read.
   * @return A {@code byte[]} that represents the file data.
   * @throws IOException
   *
   * @since 0.2
   */
  public static byte[] loadFile(File file) throws IOException {
    log.debug("loadFile(file)");

    if (file.length() > Integer.MAX_VALUE) {
      String errMsg = String.format(
          "We can't allocate a buffer with a long value:(\nMax File size: %i",
          Integer.MAX_VALUE
      );
      throw new Error(errMsg);

      // Technically, we could read much larger files by creating some interface
      // to a List<byte[]> that would allow the user to iterate the data. But
      // that's complicated bullshit for a rare situation that isn't the target
      // of this library.
    }

    DataInputStream dis =
        new DataInputStream(new FileInputStream(file));
    byte[] data = new byte[(int)file.length()];

    dis.readFully(data);
    dis.close();

    return data;
  }

  /**
   * <p>Loads a JSON resource file and returns it as a Java {@link Object}.
   * If the JSON represents an Object then the returned Java Object is an
   * instance of {@link org.json.simple.JSONObject} which can be accessed as
   * a {@link java.util.HashMap}. Similarly, if the JSON represents an Array
   * then the Java Object will be an instance of
   * {@link org.json.simple.JSONArray}.</p>
   *
   * @param clazz The {@link Class} to use for a base path.
   * @param jsonFile The JSON resource file to load.
   * @return A generic {@link Object} representation of the resource.
   */
  public static Object loadJson(Class clazz, String jsonFile)
      throws FileNotFoundException
  {
    log.debug("loadJson(clazz, jsonFile)");

    File resourceFile = resourceFile(resourceUri(clazz, jsonFile));
    String fileData = readFile(resourceFile);

    return JSONValue.parse(fileData);
  }

  /**
   * <p>Loads a JSON resource file and returns it as a Java Object of a given
   * type. The JSON must be a valid serialization of the object represented
   * by the {@code classOfT} parameter.</p>
   *
   * @param clazz The {@link Class} to use for a base path.
   * @param jsonFile The JSON resource file to load.
   * @param classOfT The class of the object that the JSON represents.
   * @param <T> The object type that will be returned.
   * @return An object of type {@code T}.
   * @throws FileNotFoundException
   */
  public static  <T> T loadJson(Class clazz, String jsonFile, Class classOfT)
      throws FileNotFoundException
  {
    log.debug("loadJson(clazz, jsonFile, classOfT)");

    File resourceFile = resourceFile(resourceUri(clazz, jsonFile));
    String fileData = readFile(resourceFile);

    Gson gson = new Gson();
    Object retObj = gson.fromJson(fileData, classOfT);

    return (T)retObj;
  }

  /**
   * Loads a text resource file and returns the contents as a {@link String}.
   *
   * @param clazz The {@link Class} to use for a base path.
   * @param textFile The text file to load.
   * @return The contents of the text file.
   * @throws FileNotFoundException
   */
  public static String loadString(Class clazz, String textFile)
      throws FileNotFoundException
  {
    log.debug("loadString(clazz, textFile)");

    File resourceFile = resourceFile(resourceUri(clazz, textFile));
    return readFile(resourceFile);
  }

  /**
   * Used by the class to read in a resource as a {@link String}.
   *
   * @param file A {@link File} object to load.
   * @return {@link String} of the file's data.
   * @throws FileNotFoundException
   */
  private static String readFile(File file) throws FileNotFoundException {
    log.debug("readFile(file)");

    StringBuilder stringBuilder = new StringBuilder();
    FileInputStream fileReader = new FileInputStream(file);
    byte[] buffer = new byte[1024];

    try {
      while (fileReader.read(buffer) != -1) {
        stringBuilder.append(new String(buffer).trim());
      }
    } catch (IOException e) {
      log.debug("Unable to read file: {}", file.getAbsoluteFile());

      e.printStackTrace();
    }

    return stringBuilder.toString();
  }

  /**
   * A simple utility method to get a {@link File} from a {@link URI}.
   *
   * @param fileUri The {@link URI} to create a {@link File} from.
   * @return The {@link File} object.
   */
  private static File resourceFile(URI fileUri) {
    log.debug("resourceFile(fileUri)");
    log.debug("URI = {}", fileUri.toString());

    File file = null;
    if (fileUri.toString().indexOf("jar:") == 0) {
      log.debug("Loading file from JAR...");

      String uriString = fileUri.toString();
      int bangIndex = uriString.indexOf("jar!") + 3;
      String pathString = uriString.substring(bangIndex + 2);

      URI jarUri = null;
      try {
        jarUri = new URI("file://" + uriString.substring(9, bangIndex));
        log.debug("JAR URI is {}", jarUri.toString());
      } catch (URISyntaxException e) {
        e.printStackTrace();
        return file;
      }

      File jarFile = new File(jarUri);
      try {
        JarInputStream jis = new JarInputStream(new FileInputStream(jarFile));
        ZipEntry ze;
        do {
          ze = jis.getNextEntry();
          log.debug("zipEntry = {}", ze.getName());
        } while (! ze.getName().equals(pathString));

        byte[] zipEntryData = new byte[(int)ze.getSize()];
        jis.read(zipEntryData);
        jis.close();

        file = File.createTempFile(
            ze.getName().substring(ze.getName().lastIndexOf('/')),
            "tmp",
            null);
        log.debug("tmpFile = {}", file.getName());

        FileOutputStream fis = new FileOutputStream(file);
        fis.write(zipEntryData);
        fis.close();
      } catch (IOException e) {
        e.printStackTrace();
        return file;
      }
    } else {
      file = new File(fileUri);
    }

    return file;
  }

  /**
   * Locates a resource using the package name of a given {@link Class} as
   * the base path and returns a {@link URI} to identify the resource.
   *
   * @param clazz The {@link Class} to use as the base path.
   * @param resource The file to be located.
   * @return A {@link URI} identifying a resource relative to the clazz.
   * @throws FileNotFoundException
   */
  private static URI resourceUri(Class clazz, String resource)
      throws FileNotFoundException
  {
    log.debug("resourceUri(clazz, resource)");

    ClassLoader classLoader = clazz.getClassLoader();
    String searchPath = clazz.getPackage().getName().replace('.', '/');
    String resourceString = String.format("%s/%s", searchPath, resource);

    log.debug("searchPath = {}", searchPath);
    log.debug("resourceString = {}", resourceString);

    URL resourceUrl = classLoader.getResource(resourceString);
    URI resourceUri = null;

    if (resourceUrl == null) {
      throw new FileNotFoundException(
          String.format("Could not locate {}", resourceString)
      );
    }

    try {
      resourceUri = resourceUrl.toURI();
    } catch (URISyntaxException usex) {
      usex.printStackTrace();
    }

    log.debug("resourceUrl = {}", resourceUrl.toString());
    log.debug("resourceUri = {}", resourceUri.toString());

    return resourceUri;
  }
}